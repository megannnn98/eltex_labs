#include <stdio.h>
#include <signal.h>
#include <sys/wait.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/errno.h>
#include <sys/sem.h>
#include <errno.h>
#include <pthread.h>

#define ATOMIC(mutex, x) do {pthread_mutex_lock(mutex);x;pthread_mutex_unlock(mutex);}while(0)

#define BARREL_START_AMOUNT 50

static int BEES_NUM = 5;
static int BEAR_FOOD_ONE_PORTION = 10;
static int BEAR_FEED_TIME = 5;

static long berrel_time = 0;

static int honey_amount = BARREL_START_AMOUNT;

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

#define MAX_SEND_SIZE sizeof(int)

static int i = 0;


void *bee_func(void *arg){


    srand((int)pthread_self());
    int t = rand() % 2;
    sleep(t+3);
	
	ATOMIC(&mutex, honey_amount += t);
	
    printf("Пчела %d шлет %d\n", *(int*)arg, t);
	
    pthread_exit(NULL);
}



void *beehive_func(void *arg){
	int result;
	int num[5] = {0,1,2,3,4};
	pthread_t thread_bee[BEES_NUM];
	
    while(1) {
		
        printf("Улей шлет %d пчел\n", BEES_NUM);
        printf("========================\n");
        
        for (i = 0; i < BEES_NUM; i++) {

            // пчела
            result = pthread_create(&thread_bee[i], NULL, bee_func, (void*)&num[i]);
            if (result != 0) {
                perror("Creating the beehive thread");
                pthread_exit(NULL);
            }
        }

        for (i = 0; i < BEES_NUM; i++) {
            pthread_join(thread_bee[i], NULL);
        }
    }

    printf("========================\n");
    printf("Улей сообщает что больше нет пчел\n");

    pthread_exit(NULL);
}

void *bear_func(void *arg){

	printf("создаем медведя\n");
    do {
        // Поели
        
		printf("Продляем ");
		printf("до %d ", honey_amount);
		ATOMIC(&mutex, honey_amount -= BEAR_FOOD_ONE_PORTION);
		printf("после %d\n", honey_amount);
        berrel_time = time(NULL) + BEAR_FEED_TIME;
        // Если есть мед, ждем время, ведь только что поели
        while (honey_amount >= BEAR_FOOD_ONE_PORTION) {

            while (berrel_time > time(NULL)) ;
            // Едим еще
			printf("Продляем ");
			printf("до %d ", honey_amount);
			ATOMIC(&mutex, honey_amount -= BEAR_FOOD_ONE_PORTION);
			printf("после %d\n", honey_amount);
            berrel_time = time(NULL) + BEAR_FEED_TIME;
        }

        printf("Нехватка меда: %d, сидит голодный\n", honey_amount);
        // Последние секунды, может чтото придет
        while (berrel_time > time(NULL)) ;

    } while (honey_amount >= BEAR_FOOD_ONE_PORTION);

    printf("Медведь мертв, меда осталось %d\n", honey_amount);

    pthread_exit(NULL);
}










int main(int argc, char* argv[])
{
    int result;
	pthread_t thread_beehive, thread_bear;

    printf("количество пчел %d\n", BEES_NUM);
    printf("порция медведя %d\n", BEAR_FOOD_ONE_PORTION);
    printf("время жизни медведя %d\n", BEAR_FEED_TIME);

    if (honey_amount < BEAR_FOOD_ONE_PORTION) {
        perror("недостаточно меда даже на раз\n");
        return EXIT_FAILURE;
    }
	
    // Улей
    result = pthread_create(&thread_beehive, NULL, beehive_func, NULL);
    if (result != 0) {
        perror("Creating the beehive thread");
        return EXIT_FAILURE;
    }

    result = pthread_create(&thread_bear, NULL, bear_func, NULL);
    if (result != 0) {
        perror("Creating the bear thread");
        return EXIT_FAILURE;
    }
	
	pthread_join(thread_bear, NULL);

    exit(0);
}
