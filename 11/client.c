#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <wait.h>

#define DO_OR_DIE(x, s) do { \
    if ((x) < 0) { \
        perror(s); \
        exit(1);   \
    } \
} while (0)


int main(int argc, char* argv[])
{
    int cfd;
    struct sockaddr_in addr;
    char ch = '3';
	pid_t pid_bee[5];
	
	while(1) {
		
		for (int i = 0; i < 5; i++) {
			DO_OR_DIE(pid_bee[i] = fork(), "fork1");
			if (0 == pid_bee[i]) {
				srand(getpid());
				sleep(rand() % 6);

				cfd = socket(AF_INET, SOCK_STREAM, 0);
				
				addr.sin_family = AF_INET;
				addr.sin_addr.s_addr = inet_addr("127.0.0.1"); /* Check for server on loopback */
				addr.sin_port = htons(4444);
				
				DO_OR_DIE(connect(cfd, (struct sockaddr*)&addr, sizeof(addr)), "connect error");
				
				ch = (rand() % 3) + '0';
				DO_OR_DIE (write(cfd, &ch, 1), "write");
				
				close(cfd);
				return 0;
			}
		}
		for (int i = 0; i < 5; i++) {
			
			waitpid(0, NULL, 0);
			usleep(10*1000);
		}
	
	}
	
    return 0;
}
