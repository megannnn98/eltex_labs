#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>

#define DO_OR_DIE(x, s) do { \
    if ((x) < 0) { \
        perror(s); \
        exit(1);   \
    } \
} while (0)

	
static int honey = 50;
int fd[2] = {0};
int sfd, cfd;
int hungry = 0;
	
void handle_sigalarm(int sig)
{
	if (honey >= 10) {
		hungry = 0;
		honey -= 10;
		printf("alarm honey = %d\n", honey);
		alarm(3); 
	} else if (!hungry){
		hungry = 1;
		honey -= 10;
		printf("alarm honey = %d\n", honey);
		alarm(3); 
	} else {
		printf("bear is dead\n");
		shutdown(cfd, SHUT_RDWR);
		close(fd[1]);
		kill(getpid(), SIGTERM);
	}
}

	
int main()
{
    
    socklen_t len;
    char ch, buff[INET_ADDRSTRLEN];
	
	
    struct sockaddr_in saddr, caddr;
	int childpid;

	
    DO_OR_DIE(sfd = socket(AF_INET, SOCK_STREAM, 0), "ERROR opening socket");

    saddr.sin_family = AF_INET;
    saddr.sin_addr.s_addr = htonl(INADDR_ANY);
    saddr.sin_port = htons(4444);

    DO_OR_DIE(bind(sfd, (struct sockaddr*)&saddr, sizeof(saddr)), "bind");

    listen(sfd, 5);
	
	signal(SIGALRM , handle_sigalarm); /* Setup handler for 
	 alarm signal to override the default behaviour which would be to 
	 end the process . */

	alarm(3); 
    printf("Server waiting\n");
		
    while (1) {

        len = sizeof(caddr);
        cfd = accept(sfd, (struct sockaddr*)&caddr, &len);
		
		pipe(fd);

		DO_OR_DIE(childpid = fork(), "fork");
        if (0 == childpid) {
			close(fd[0]);
//            printf("Child Server Created Handling connection with %s\n",
//                inet_ntop(AF_INET, &caddr.sin_addr, buff, sizeof(buff)));

            close(sfd);

			DO_OR_DIE (read(cfd, &ch, 1), "read");
			printf("net rx:%c\n", ch);			
			DO_OR_DIE (write(fd[1], &ch, 1), "write");
			
			shutdown(cfd, SHUT_RDWR);
			close(fd[1]);
            close(cfd);
            return 0;
        }
		close(fd[1]);
		
		DO_OR_DIE(read(fd[0], &ch, 1), "read");
		
		honey += atoi(&ch);
		printf("honey %d\n", honey);
		
		close(fd[0]);
        close(cfd);
    }
	return 0;
}