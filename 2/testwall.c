
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 1024 /* максимальная длина строки */

typedef struct {
    char* mas;
    int num;
} A_t;

A_t* array;

int num_words(char* s)
{
    unsigned int a, i;
    a = 0;
    for (i = 0; i < strlen(s) - 1; i++)
        if ((s[i] == ' ') && (s[i + 1] != ' '))
            a = a + 1;
    if (s[0] != ' ')
        a = a + 1;

    return a;
}

/** length – длина строки 
 * string – введенная строка 
 * maxlen – максимально возможная длина строки (размерность массива string) 
*/ 
int inp_str(char* string, int maxlen)
{
    fgets(string, maxlen, stdin); // читаем строку в буфер
    return strlen(string);
}
/** string – выводимая строка 
 * length – длина строки 
 * number – номер строки 
*/ 
void out_str(char* string, int length, int number)
{
    printf("%s", string);
}

void read_array(int count)
{
    char buffer[MAX_LEN] = "hello there";
    int length;

    printf("\n");

    array = (A_t*)malloc(sizeof(A_t) * count); // выделяем память для массива указателей
    for (int i = 0; i < count; i++) {
        //
	    printf("input %d string:", i+1);
        length = inp_str(buffer, MAX_LEN); 
        array[i].mas = (char*)malloc(sizeof(char) * length); //выделяем память для строки
        strcpy(array[i].mas, buffer); //копируем строку из буфера в массив указателей
        array[i].num = num_words(array[i].mas);
    }
}

void print_array(A_t* A, int count)
{
    printf("\n");
    for (int i = 0; i < count; i++) {
         out_str(A[i].mas, strlen(A[i].mas), i);
    }
}

void free_array(A_t* array, int count)
{
    for (int i = 0; i < count; i++) {
        free(array[i].mas); // освобождаем память для отдельной строки
    }
    free(array); // освобождаем памать для массива указателей на строки
}

void swap(A_t* xp, A_t* yp)
{
    A_t temp = *xp;
    *xp = *yp;
    *yp = temp;
}

// A function to implement bubble sort
int bubbleSort(A_t arr[], int n)
{
    int i, j, swaps = 0;
    for (i = 0; i < n - 1; i++)

        // Last i elements are already in place
        for (j = 0; j < n - i - 1; j++) {
            if (arr[j].num > arr[j + 1].num) {
                swap(&arr[j], &arr[j + 1]);
                swaps++;
            }
        }
    return swaps;
}


int main(int argc, char **argv){

    int count = 3;
    int swaps = 0;
    char sym = 0;
    char s[10] = {0};

    printf("\ninput num of strings:");

    inp_str(s, 10);
    count = atoi(s);

    printf("\nnum of strings:%d", count);

    read_array(count);

    printf("\nsort...");
    swaps = bubbleSort(array, count);
    printf("\nsorted\n");

    sym = array[count-1].mas[0];

    printf("===============================\n");

    print_array(array, count);

    printf("\n===============================\n");

    printf("result:\nnum swaps = %d\nfirst sym of the last string = %c\n", swaps, sym);

    free_array(array, count);
    printf("memory free\n\n");
}
