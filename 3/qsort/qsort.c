#include <stdio.h>
#include <stdlib.h>

typedef struct {
	char name[50];
	int balls;
} student_t;

void read_student(student_t* st) 
{
	printf("Insert name: ");
	scanf("%s", st->name);
	printf("insert balls: ");
	scanf("%d", &st->balls);
}

static int cmp(const void* p1, const void* p2)
{
	student_t* st1 = *(student_t**)p1;
	student_t* st2 = *(student_t**)p2;
	return -st2->balls + st1->balls;
}

int main(int argc, char** argv) {
	int count = 3;
	printf("insert num student_t :");
	scanf("%d", &count);
	
	student_t** st = (student_t**)malloc(sizeof(student_t*)*count);
	for(int i = 0; i < count; i++) {
		st[i] = (student_t*)malloc(sizeof(student_t));
		read_student(st[i]);
	}
	qsort(st, count, sizeof(student_t*), cmp);
	printf("\n\nname: %s\n", st[0]->name);
	printf("ball: %d\n", st[0]->balls);
	
	for(int i = 0; i < count; i++) {
		free(st[i]);
	}
	free(st);
	return 0;
}
