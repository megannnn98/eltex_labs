
#include <stdio.h>
#include <stdlib.h>

typedef struct 
{
    unsigned int day ;
    unsigned int month;
    unsigned int year;
} date_t;


typedef struct {
  char name[40];
  date_t date;
  int cost;
  int quantity;
} purchase_t;


 
void read_one_student(purchase_t *pc){

    printf("\nВведите название покупки:");
    scanf("%s", pc->name);
    printf("\nВведите день приобретения:");
    scanf("%d", &pc->date.day);
    printf("\nВведите месяц приобретения:");
    scanf("%d", &pc->date.month);
    printf("\nВведите год приобретения:");
    scanf("%d", &pc->date.year);
    printf("\nВведите цену:");
    scanf("%d", &pc->cost);
    printf("\nВведите количество:");
    scanf("%d", &pc->quantity);
}

void print_one_student(purchase_t *pc){

    printf("\n");
    printf("\nНазвание покупки:%s", pc->name);
    printf("\nдень приобретения:%d", pc->date.day);
    printf("\nмесяц приобретения:%d", pc->date.month);
    printf("\nгод приобретения:%d", pc->date.year);
    printf("\nцена:%d", pc->cost);
    printf("\nколичество:%d", pc->quantity);
}



static int cmp(const void *p1, const void *p2){
    purchase_t* pc1 = *(purchase_t**)p1;
    purchase_t* pc2 = *(purchase_t**)p2;

    return pc1->date.month - pc2->date.month;
}

int main(int argc, char **argv){
  
    int count = 3;
    printf("\nВведите кол-во покупок:");
    scanf("%d", &count);
    
    purchase_t** pc = (purchase_t**)malloc(sizeof(purchase_t*)*count);
    for (int i = 0; i < count ; i++){

        pc[i] = (purchase_t*) malloc (sizeof(purchase_t));
        read_one_student(pc[i]);
    }

    printf("\nДо сортировки:");
    printf("\n===========================");

    for (int i = 0; i < count ; i++){
        print_one_student(pc[i]);
    }


    qsort(pc, count, sizeof(purchase_t*), cmp);

    printf("\nПосле сортировки месяцам:");
    printf("\n===========================");

    for (int i = 0; i < count ; i++){
        print_one_student(pc[i]);
    }

    printf("\n===========================");
    printf("\n");

    for (int i = 0; i < count; i++)
    {
        free(pc[i]);
    }
    free(pc);
    return 0;
}