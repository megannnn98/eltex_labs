
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXLEN 1024


int space_count(char* ptr)
{
    int k = 0;

    while (*ptr)
    {
        if (*ptr++ == ' ') k++;
    }
    return k;
}


int main(int argc, char ** argv){
    FILE *fp1 = NULL;
    FILE *fp2 = NULL;
    char s[MAXLEN] = {0};

    if (argc < 2){
        fprintf (stderr, "Мало аргументов. Используйте <имя файла>\n");
        exit (1);
    }

    if ( (fp1 = fopen(argv[1], "r")) == NULL ) {
	    printf("error open 1\n");
	    exit(0);
    }
    if ( (fp2 = fopen("outfile.dat", "w")) == NULL ) {
        printf("error open 2\n");
        exit(0);
    }

    printf("start\n");

    while ( !feof(fp1) ) {
        fgets(s, MAXLEN, fp1);
        if (space_count(s) > atoi(argv[2])) {
            continue;
        }
        fwrite(s, sizeof(char), strlen(s), fp2);
    }

    printf("done\n");

    fclose(fp1);
}
