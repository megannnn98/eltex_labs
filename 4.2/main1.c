
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXLEN 1024


int main(int argc, char ** argv){
    FILE *in = NULL;
    FILE *out = NULL;
    char c = 0;

    if (argc < 2){
        fprintf (stderr, "Мало аргументов. Используйте <имя файла>\n");
        exit (1);
    }

    if ( (in = fopen(argv[1], "r")) == NULL ) {
	    printf("error open 1\n");
	    exit(0);
    }
    if ( (out = fopen("outfile.dat", "w")) == NULL ) {
        printf("error open 2\n");
        exit(0);
    }

    printf("start\n");

    while (EOF != (c = fgetc(in)))
    {
        if (' ' == c) {
            fputc(c, out);
        }
        fputc(c, out);
    }

    printf("done\n");

    fclose(in);
}
