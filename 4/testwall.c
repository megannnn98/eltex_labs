
#include <stdio.h>
#include <stdlib.h>

typedef struct 
{
    unsigned char day ;
    unsigned char month;
    unsigned short year;
} date_t;


typedef struct {
  char name[40];
  date_t date;
  int cost;
  int quantity;
} purchase_t;


void swap(purchase_t* xp, purchase_t* yp)
{
    purchase_t temp = *xp;
    *xp = *yp;
    *yp = temp;
}

// A function to implement bubble sort
int bubble_sort(purchase_t* arr[], int n)
{
    int i, j, swaps = 0;
    for (i = 0; i < n - 1; i++)

        // Last i elements are already in place
        for (j = 0; j < n - i - 1; j++) {
            if (arr[j]->date.month > arr[j + 1]->date.month) {
                swap(arr[j], arr[j + 1]);
                swaps++;
            }
        }
    return swaps;
}



 
void read_one_student(purchase_t *pc){/*
    printf("Введите название пакупки:");
    scanf("%s", pc->name);
    printf("Введите дату приобретения день, месяц, год:");
    scanf("%d", &pc->date.day);
    scanf("%d", &pc->date.month);
    scanf("%d", &pc->date.year);
    printf("Введите цену пакупки:");
    scanf("%d", &pc->cost);
    printf("Введите количество:");
    scanf("%d", &pc->quantity);*/
  
    static int i=50;
    
    i--;
    sprintf(pc->name, "name");
    pc->date.day = i;
    pc->date.month = i;
    pc->date.year = i;
    pc->cost = i;
    pc->quantity = i;    
}



int main(int argc, char **argv){
  
    int count = 10;
    printf("Введите кол-во студентов:");
    //scanf("%d", &count);
    
    purchase_t** pc = (purchase_t**)malloc(sizeof(purchase_t*)*count);
    for (int i = 0; i < count ; i++){
        pc[i] = (purchase_t*) malloc (sizeof(purchase_t));
        read_one_student(pc[i]);
    }
    
    
    for (int i = 0; i < count ; i++){
        printf("Название:%s\n", pc[i]->name);
        printf("Дата: %d, %d, %d\n", pc[i]->date.day, pc[i]->date.month, pc[i]->date.year);
        printf("Цена:%d\n", pc[i]->cost);
        printf("Количество:%d\n", pc[i]->quantity);
    }
    
    printf("После сортировки\n");
    
    bubble_sort(pc, count); 
    
    
    for (int i = 0; i < count; i++)
    {
        free(pc[i]);
    }
    free(pc);
    return 0;
}