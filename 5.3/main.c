
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include "my_math.h"


int main(int argc, char ** argv){

	void *handle;
	double (*any_pow)(double);
	char *error;
	handle = dlopen("./libmath.so", RTLD_LAZY);
	if (!handle) {
		fputs(dlerror(), stderr);
		printf("\n");
		exit(1);
	}
	any_pow = dlsym(handle, "pow3");
	if ((error = dlerror()) != NULL) {
		fprintf(stderr, "%s\n", error);
		printf("\n");
		exit(1);
	}
	printf("%f\n", (*any_pow)(2.0));
	any_pow = dlsym(handle, "pow4");
	if ((error = dlerror()) != NULL) {
		fprintf(stderr, "%s\n", error);
		printf("\n");
		exit(1);
	}
	printf("%f\n", (*any_pow)(2.0));
	dlclose(handle);

}
