#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <wait.h>

int main(int argc, char* argv[]) {

    pid_t child_pid = fork();	/* fork returns type pid_t */
    int N = 0;
    int status = 0, result = 0;

    if (child_pid < 0) {
        printf("error\n");
        exit(1);
    }
    if (0 == child_pid) {

        sleep(5);
        kill(getppid(), SIGTERM);
        printf("kill parent signal\n");
        exit(0);
    }
    do {
        printf("\nparent: insert number:");
        scanf("%d", &N);
        printf("\nparent survived"); 

        kill(child_pid, SIGCONT);

        result = waitpid(child_pid, &status, WUNTRACED | WCONTINUED);
        if (-1 == result) {
            perror("waitpid");
            exit(EXIT_FAILURE);
        }

    } while(!WIFEXITED(status) && !WIFSIGNALED(status));


    exit(EXIT_SUCCESS);
}
