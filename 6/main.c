#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <time.h>

#define BARREL_START_AMOUNT 20

static int BEES_NUM = 5;
static int BEAR_FOOD_ONE_PORTION = 10;
static int BEAR_FEED_TIME = 5;

static long berrel_time = 0;
static long bear_time = 0;

static int honey_amount = BARREL_START_AMOUNT;
static int honey_plus_flag = 0;

void signal_handler(int signo)
{

    if (SIGUSR1 == signo) {
        honey_plus_flag = 1;
        printf("---> мед++\n");
    }
    else {
        printf("неизвестный сигнал \n");
    }
}

int main(int argc, char* argv[])
{
    pid_t pid_beehive, pid_bear, pid_bee[BEES_NUM];

	printf("введено %d параметров \n", argc);

	if (argc != 4) {

		printf("нужно 3 параметра: \n");
		printf("количество пчел(2) \n");
		printf("порция медведя(10) \n");
		printf("время жизни медведя(5) \n");
		exit(1);
	}

	BEES_NUM = atoi(argv[1]);
	BEAR_FOOD_ONE_PORTION = atoi(argv[2]);
	BEAR_FEED_TIME = atoi(argv[3]);

	if ((BEES_NUM <= 0) ||
		(BEAR_FOOD_ONE_PORTION <= 0) ||
		(BEAR_FEED_TIME <= 0))
	{
		printf("ошибка данных \n");
		exit(1);
	}
    printf("количество пчел %d\n", BEES_NUM);
    printf("порция медведя %d\n", BEAR_FOOD_ONE_PORTION);
    printf("время жизни медведя %d\n", BEAR_FEED_TIME);

    // Улей
    if (0 == (pid_beehive = fork())) {

        printf("Улей старт %d бочка %d\n", getpid(), getppid());

        printf("========================\n");
        int i = 0;
        for (i = 0; i < BEES_NUM; i++) {

            // пчела
            if (0 == (pid_bee[i] = fork())) {
                printf("Пчела %d старт\n", i);
                srand(getpid());
                sleep(rand() % 2);

                exit(0);
            }
        }

        for (i = 0; i < BEES_NUM; i++) {
           	waitpid(pid_bee[i], NULL, 0);
            //wait(NULL);
            usleep(10*1000);
            kill(getppid(), SIGUSR1);
            printf("Сигнал от улья в бочку %d\n", i);
        }
        printf("========================\n");
        exit(0);
    }

    printf("Бочка старт %d\n", getpid());

    if (SIG_ERR == signal(SIGUSR1, signal_handler)) {
        perror("signal set err");
        exit(1);
    }

    if (honey_amount < BEAR_FOOD_ONE_PORTION) {
        perror("недостаточно меда даже на раз\n");
        exit(1);
    }

    do {
        // Поели
        printf("Продляем медведя, меда осталось %d\n", honey_amount);
        honey_amount -= BEAR_FOOD_ONE_PORTION;
        berrel_time = time(NULL) + BEAR_FEED_TIME;
        // Если есть мед, ждем время, ведь только что поели
        while (honey_amount >= BEAR_FOOD_ONE_PORTION) {

            while (berrel_time > time(NULL)) {
                if (honey_plus_flag) {
                    honey_plus_flag = 0;
                    honey_amount++;
                }
            }
            // Едим еще
            printf("Продляем медведя, меда осталось %d\n", honey_amount);
            honey_amount -= BEAR_FOOD_ONE_PORTION;
            berrel_time = time(NULL) + BEAR_FEED_TIME;
        }

        printf("Нехватка меда: %d, сидит голодный\n", honey_amount);
        // Последние секунды, может чтото придет
        while (berrel_time > time(NULL)) {
            if (honey_plus_flag) {
                honey_plus_flag = 0;
                honey_amount++;
                printf("прилетела пчела\n");
            }
        }

    } while (honey_amount >= BEAR_FOOD_ONE_PORTION);

    printf("Медведь мертв, меда осталось %d\n", honey_amount);
    kill (pid_beehive, SIGSTOP);
    kill (pid_beehive, SIGTERM);

    exit(0);
}
