#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <time.h>

#define _BSD_SOURCE

#define DO_OR_DIE(x, s) do { \
    if ((x) < 0) { \
        perror(s); \
        exit(1);   \
    } \
} while (0)

#define BARREL_START_AMOUNT 50

static int BEES_NUM = 5;
static int BEAR_FOOD_ONE_PORTION = 10;
static int BEAR_FEED_TIME = 5;

static long berrel_time = 0;
static long bear_time = 0;

static int honey_amount = BARREL_START_AMOUNT;
static int honey_plus_flag = 0;


int main(int argc, char* argv[])
{
    pid_t pid_beehive, pid_bear, pid_bee[BEES_NUM];

    printf("количество пчел %d\n", BEES_NUM);
    printf("порция медведя %d\n", BEAR_FOOD_ONE_PORTION);
    printf("время жизни медведя %d\n", BEAR_FEED_TIME);


    int fd[2] = {0};
    pipe(fd);

    // Улей
    DO_OR_DIE(pid_beehive = fork(), "fork1");
    if (0 == pid_beehive) {

        printf("========================\n");
        while(1) {

            printf("Улей pid %d pid %d\n", getpid(), getppid());

            int i = 0;
            for (i = 0; i < BEES_NUM; i++) {

                // пчела
                DO_OR_DIE(pid_bee[i] = fork(), "fork1");
                if (0 == pid_bee[i]) {
                    close(fd[0]);

                    srand(getpid());
                    int t = rand() % 6;
                    sleep(t);
                    write(fd[1], &t, sizeof(t));
                    printf("Пчела %d шлет %d\n", i, t);
                    close(fd[1]);
                    exit(0);
                }
            }

            for (i = 0; i < BEES_NUM; i++) {
                //waitpid(pid_bee[i], NULL, 0);
                waitpid(0, NULL, 0);
                usleep(10*1000);
            }
        }

        printf("========================\n");
        exit(0);
    }

    printf("Бочка старт %d\n", getpid());


    if (honey_amount < BEAR_FOOD_ONE_PORTION) {
        perror("недостаточно меда даже на раз\n");
        exit(1);
    }

    close(fd[1]);
    int t = 0;
    do {
        // Поели
        printf("Продляем медведя, меда осталось %d\n", honey_amount);
        honey_amount -= BEAR_FOOD_ONE_PORTION;
        berrel_time = time(NULL) + BEAR_FEED_TIME;
        // Если есть мед, ждем время, ведь только что поели
        while (honey_amount >= BEAR_FOOD_ONE_PORTION) {

            while (berrel_time > time(NULL)) {
                if (read(fd[0], &t, sizeof(t))) {
                    honey_amount += t;
                }
            }
            // Едим еще
            printf("Продляем медведя, меда осталось %d\n", honey_amount);
            honey_amount -= BEAR_FOOD_ONE_PORTION;
            berrel_time = time(NULL) + BEAR_FEED_TIME;
        }

        printf("Нехватка меда: %d, сидит голодный\n", honey_amount);
        // Последние секунды, может чтото придет
        while (berrel_time > time(NULL)) {
            if (read(fd[0], &t, sizeof(t))) {
                honey_amount += t;
                printf("прилетела пчела\n");
            }
        }

    } while (honey_amount >= BEAR_FOOD_ONE_PORTION);

    close(fd[0]);

    printf("Медведь мертв, меда осталось %d\n", honey_amount);
    kill (pid_beehive, SIGSTOP);
    kill (pid_beehive, SIGTERM);

    exit(0);
}
