#include <stdio.h>
#include <signal.h>
#include <sys/wait.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>


#define DO_OR_DIE(x, s) do { \
    if ((x) < 0) { \
        perror(s); \
        exit(1);   \
    } \
} while (0)

#define BARREL_START_AMOUNT 50

static int BEES_NUM = 5;
static int BEAR_FOOD_ONE_PORTION = 10;
static int BEAR_FEED_TIME = 5;

static long berrel_time = 0;
static long bear_time = 0;

static int honey_amount = BARREL_START_AMOUNT;
static int honey_plus_flag = 0;


#define MAX_SEND_SIZE sizeof(int)

struct mymsgbuf {
    long mtype;
    int data;
};

int msgqid, rc;

void send_message(int qid, struct mymsgbuf *qbuf, long type, int data) {
    qbuf->mtype = type;
    qbuf->data = data;

    DO_OR_DIE(msgsnd(qid, (struct msgbuf *)qbuf, sizeof(data), 0), "msgsnd");
}

int read_message(int qid, struct mymsgbuf *qbuf, long type) {
    qbuf->mtype = type;
    int result;
    errno = 0;
    result = msgrcv(qid, (struct msgbuf *)qbuf, MAX_SEND_SIZE, type, IPC_NOWAIT);

    if (result < 0) {
        if ((errno != ENOMSG) && (errno != 0)) {
            printf("результат %d, errno = %d\n", result, errno);
        }

        switch(errno)
        {
            case E2BIG: printf("%s\n", "E2BIG:"); break;
            case EACCES:printf("%s\n", "EACCES:"); break;
            case EFAULT:printf("%s\n", "EFAULT:"); break;
            case EIDRM:printf("%s\n", "EIDRM:"); break;
            case EINTR:printf("%s\n", "EINTR:"); break;
            case EINVAL:printf("%s\n", "EINVAL:"); break;
            //case ENOMSG :printf("%s\n", "ENOMSG:"); break;
        }
    }

    return result;
}



int main(int argc, char* argv[])
{
    pid_t pid_beehive, pid_bear, pid_bee[BEES_NUM];
    key_t key;
    int qtype = 1;
    struct mymsgbuf qbuf;

    printf("количество пчел %d\n", BEES_NUM);
    printf("порция медведя %d\n", BEAR_FOOD_ONE_PORTION);
    printf("время жизни медведя %d\n", BEAR_FEED_TIME);


    key = ftok(".", 'm');
    DO_OR_DIE((msgqid = msgget(key, IPC_CREAT|0660)), "msgget");

    // Улей
    DO_OR_DIE(pid_beehive = fork(), "fork1");
    if (0 == pid_beehive) {

        while(1) {
            printf("Улей pid %d Медведь pid %d\n", getpid(), getppid());

            printf("========================\n");
            int i = 0;
            for (i = 0; i < BEES_NUM; i++) {

                // пчела
                DO_OR_DIE(pid_bee[i] = fork(), "fork1");
                if (0 == pid_bee[i]) {

                    srand(getpid());
                    int t = rand() % BEES_NUM;
                    sleep(t);

                    printf("Пчела %d шлет %d\n", i, t);
                    send_message(msgqid, (struct mymsgbuf *)&qbuf, qtype, t);

                    exit(0);
                }
            }

            for (i = 0; i < BEES_NUM; i++) {
                //waitpid(pid_bee[i], NULL, 0);
                waitpid(0, NULL, 0);
                usleep(10*1000);
            }
        }

        printf("========================\n");

        int t = -1;
        printf("Улей сообщает что больше нет пчел\n");
        send_message(msgqid, (struct mymsgbuf *)&qbuf, qtype, t);

        exit(0);
    }

    printf("Бочка старт %d\n", getpid());


    if (honey_amount < BEAR_FOOD_ONE_PORTION) {
        perror("недостаточно меда даже на раз\n");
        exit(1);
    }


    int t = 0, block = 0;
    do {
        // Поели
        printf("Продляем медведя, меда осталось %d\n", honey_amount);
        honey_amount -= BEAR_FOOD_ONE_PORTION;
        berrel_time = time(NULL) + BEAR_FEED_TIME;
        // Если есть мед, ждем время, ведь только что поели
        while (honey_amount >= BEAR_FOOD_ONE_PORTION) {

            while (berrel_time > time(NULL)) {

                if (0 == block) {
                    int x;
                    x = read_message(msgqid, &qbuf, qtype);

                    if (x == -1) {
                        continue;
                    }
                    if (-1 == qbuf.data) {
                        block = 1;
                    } else {
                        t = qbuf.data;
                        honey_amount += t;
                    }

                }
            }
            // Едим еще
            printf("Продляем медведя, меда осталось %d\n", honey_amount);
            honey_amount -= BEAR_FOOD_ONE_PORTION;
            berrel_time = time(NULL) + BEAR_FEED_TIME;
        }

        printf("Нехватка меда: %d, сидит голодный\n", honey_amount);
        // Последние секунды, может чтото придет
        while (berrel_time > time(NULL)) {
               if (0 == block) {
                    int x;
                    x = read_message(msgqid, &qbuf, qtype);

                    if (x == -1) {
                        continue;
                    }
                    if (-1 == qbuf.data) {
                        block = 1;
                    } else {
                        t = qbuf.data;
                        honey_amount += t;
                    }

                }
        }

    } while (honey_amount >= BEAR_FOOD_ONE_PORTION);

    printf("Медведь мертв, меда осталось %d\n", honey_amount);
    kill (pid_beehive, SIGSTOP);
    kill (pid_beehive, SIGTERM);

    DO_OR_DIE((rc = msgctl(msgqid, IPC_RMID, NULL)), "can't delete msg");

    exit(0);
}
