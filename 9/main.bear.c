#include <stdio.h>
#include <signal.h>
#include <sys/wait.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/errno.h>
#include <sys/shm.h>
#include <sys/sem.h>


#define DO_OR_DIE(x, s) do { \
    if ((x) < 0) { \
        perror(s); \
        exit(1);   \
    } \
} while (0)

#define BARREL_START_AMOUNT 50

static int BEES_NUM = 5;
static int BEAR_FOOD_ONE_PORTION = 10;
static int BEAR_FEED_TIME = 5;

static long berrel_time = 0;
static long bear_time = 0;

static int honey_amount = BARREL_START_AMOUNT;
static int honey_plus_flag = 0;


#define MAX_SEND_SIZE sizeof(int)

union semun {
	int val;
	struct semid_ds *buf;
	unsigned short *array;
	struct seminfo *__buf;
};



int main(int argc, char* argv[])
{
    pid_t pid_beehive, pid_bear, pid_bee[BEES_NUM];
    key_t key;
    int semid;
    int *shm, *s, shmid;
    union semun arg;
    struct sembuf lock_res = {0, -1, 0};
    struct sembuf release_res = {0, 1, 0};
    struct sembuf wait_res = {0, 0, 0};

    printf("количество пчел %d\n", BEES_NUM);
    printf("порция медведя %d\n", BEAR_FOOD_ONE_PORTION);
    printf("время жизни медведя %d\n", BEAR_FEED_TIME);


    DO_OR_DIE(key = ftok(".", 'm'), "key error");

    DO_OR_DIE(semid = semget(key, 1, IPC_CREAT | 0666), "semget");
    DO_OR_DIE(shmid = shmget(key, sizeof(int), IPC_CREAT | 0666), "shmget");


    arg.val = 1;
    semctl(semid, 0, SETVAL, arg);

    // Улей
    DO_OR_DIE(pid_beehive = fork(), "fork1");
    if (0 == pid_beehive) {

        while(1) {
            printf("Улей pid %d Медведь pid %d\n", getpid(), getppid());

            printf("========================\n");
            int i = 0;
            for (i = 0; i < BEES_NUM; i++) {

                // пчела
                DO_OR_DIE(pid_bee[i] = fork(), "fork1");
                if (0 == pid_bee[i]) {

                    srand(getpid());
                    int t = rand() % BEES_NUM;
                    sleep(t);

                    // Получим доступ к памяти
				    if ((int*)-1 == (shm = shmat(shmid, NULL, 0660))) {
				    	perror("shmat");
				    	exit(1);
				    }

				    //printf("Доступ к shm запрещен\n");
				    DO_OR_DIE(semop(semid, &lock_res, 1), "Lock failed");
                    
                    int *s = shm;
                    *s += t;
                    printf("Пчела %d шлет %d\n", i, t);

					DO_OR_DIE(semop(semid, &release_res, 1), "Release failed");

					//printf("Доступ к shm разрешен\n");
					
                    DO_OR_DIE(shmdt(shm), "Ошибка отключения");

                    exit(0);
                }
            }

            for (i = 0; i < BEES_NUM; i++) {
                //waitpid(pid_bee[i], NULL, 0);
                waitpid(0, NULL, 0);
                usleep(10*1000);
            }
        }

        printf("========================\n");

        int t = -1;
        printf("Улей сообщает что больше нет пчел\n");

        exit(0);
    }


    if (honey_amount < BEAR_FOOD_ONE_PORTION) {
        perror("недостаточно меда даже на раз\n");
        exit(1);
    }


    if ((int*)-1 == (shm = shmat(shmid, NULL, 0660))) {
    	perror("shmat");
    	exit(1);
    }

    *shm = honey_amount;

    do {
        // Поели
        printf("Продляем медведя, меда осталось %d\n", *shm);
        *shm -= BEAR_FOOD_ONE_PORTION;
        berrel_time = time(NULL) + BEAR_FEED_TIME;
        // Если есть мед, ждем время, ведь только что поели
        while (*shm >= BEAR_FOOD_ONE_PORTION) {

            while (berrel_time > time(NULL)) ;
            // Едим еще
            printf("Продляем медведя, меда осталось %d\n", *shm);
            *shm -= BEAR_FOOD_ONE_PORTION;
            berrel_time = time(NULL) + BEAR_FEED_TIME;
        }

        printf("Нехватка меда: %d, сидит голодный\n", *shm);
        // Последние секунды, может чтото придет
        while (berrel_time > time(NULL)) ;

    } while (*shm >= BEAR_FOOD_ONE_PORTION);

    printf("Медведь мертв, меда осталось %d\n", *shm);
    kill (pid_beehive, SIGSTOP);
    kill (pid_beehive, SIGTERM);

    DO_OR_DIE(shmdt(shm), "Ошибка отключения");
    DO_OR_DIE(shmctl(shmid, IPC_RMID, 0), "Невозможно удалить область\n");
    DO_OR_DIE(semctl(semid, 0, IPC_RMID), "Невозможно удалить семафот\n");

    exit(0);
}
