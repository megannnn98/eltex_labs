/*
4. Warcraft. Заданное количество юнитов добывают золото равными порциями из одной шахты, 
задерживаясь в пути на случайное время, до ее истощения. 
Работа каждого юнита реализуется в порожденном процессе.
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <wait.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/sem.h>


#define DO_OR_DIE(x, s) do { \
    if ((x) < 0) { \
        perror(s); \
        exit(1);   \
    } \
} while (0)

#define BARREL_START_AMOUNT 50
#define MAX_SEND_SIZE sizeof(int)

static int UNIT_NUM = 5;


union semun {
	int val;
	struct semid_ds *buf;
	unsigned short *array;
	struct seminfo *__buf;
};



int main(int argc, char* argv[])
{
    pid_t pid_coalpid, pid_unit[UNIT_NUM];
    key_t key;
    int semid;
    int *shm, *s, shmid;
    union semun arg;
    struct sembuf lock_res = {0, -1, 0};
    struct sembuf release_res = {0, 1, 0};
    struct sembuf wait_res = {0, 0, 0};

    printf("количество юнитов %d\n", UNIT_NUM);

    DO_OR_DIE(key = ftok(".", 'm'), "key error");

    DO_OR_DIE(semid = semget(key, 1, IPC_CREAT | 0666), "semget");
    DO_OR_DIE(shmid = shmget(key, sizeof(int), IPC_CREAT | 0666), "shmget");


    arg.val = 1;
    semctl(semid, 0, SETVAL, arg);

    // Улей
    DO_OR_DIE(pid_coalpid = fork(), "fork1");
    if (0 == pid_coalpid) {

		// Получим доступ к памяти
		if ((int*)-1 == (shm = shmat(shmid, NULL, 0660))) {
			perror("shmat");
			exit(1);
		}
        while(*shm) {
            printf("Дом юнитов pid %d Шахта pid %d\n", getpid(), getppid());

            printf("========================\n");
            int i = 0;
            for (i = 0; i < UNIT_NUM && *shm; i++) {

                // пчела
                DO_OR_DIE(pid_unit[i] = fork(), "fork1");
                if (0 == pid_unit[i]) {

                    srand(getpid());
                    int t = rand() % UNIT_NUM;
                    sleep(t);

                    // Получим доступ к памяти
				    if ((int*)-1 == (shm = shmat(shmid, NULL, 0660))) {
				    	perror("shmat");
				    	exit(1);
				    }

				    //printf("Доступ к shm запрещен\n");
				    DO_OR_DIE(semop(semid, &lock_res, 1), "Lock failed");
                    
                    int *s = shm;
                    if (*s >= t) {
                    	*s -= t;
                	} else {
						printf("Нехватает золота для юнита\n");
						
						sleep(1);
						kill (pid_coalpid, SIGSTOP);
						kill (pid_coalpid, SIGTERM);
						
					}
                    printf("Юнит %d берет золото %d\n", i, t);

					DO_OR_DIE(semop(semid, &release_res, 1), "Release failed");

					//printf("Доступ к shm разрешен\n");
					
                    DO_OR_DIE(shmdt(shm), "Ошибка отключения");

                    exit(0);
                }
            }

            for (i = 0; i < UNIT_NUM; i++) {
                waitpid(0, NULL, 0);
            }
        }

        printf("========================\n");

        printf("Конец дома юнитов\n");

        exit(0);
    }

    if ((int*)-1 == (shm = shmat(shmid, NULL, 0660))) {
    	perror("shmat");
    	exit(1);
    }

    *shm = BARREL_START_AMOUNT;
	int t = *shm;
    while (*shm > 0) {
		
		if (t != *shm) {
			t = *shm;
			printf("Шахта %d\n", *shm);
		}
	}

    printf("Шахта пуста %d\n", *shm);
	sleep(1);
	kill (pid_coalpid, SIGSTOP);
	kill (pid_coalpid, SIGTERM);

    DO_OR_DIE(shmdt(shm), "Ошибка отключения");
    DO_OR_DIE(shmctl(shmid, IPC_RMID, 0), "Невозможно удалить область\n");
    DO_OR_DIE(semctl(semid, 0, IPC_RMID), "Невозможно удалить семафот\n");

    exit(0);
}
