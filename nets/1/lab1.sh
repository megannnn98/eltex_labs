#!/bin/bash 

#$1 В какой папке создавать 
#$2 сколько папок на первом уровне
#$3 сколько в каждой из первого уровня
#$4 шаблон имени


if [ $# -ne 4 ]; then
	echo "4 parameters"
	exit -1;
fi


cd "$1"
for (( i=0; i < "$2"; i++ ))
do 
	if ! [ -d "$i" ]; then
		mkdir "$i"
		cd "$i"
		
		for (( j=0; j < "$3"; j++ ))
		do 
			if ! [ -d "$j" ]; then
				mkdir "$j"
				cd "$j"
				
				for (( k=0; k < 20; k++ ))
				do 
					if ! [ -f "$4_$k" ]; then
						touch "$4_$k" 
					else 
						echo "file exist"
					fi
				done
				cd ..	
			else 
				echo "dir $j exist"
			fi
		done	
		cd ..	
	else 
		echo "dir $i exist"
	fi
done

#	for (( j=0; j < 10; j++ ))
#	do 
#		for (( k=0; k < 20; k++ ))
#		do 
#			touch "number_is_$j" 
#		done
#	done

