#/bin/bash

#sed '/#/d' infile.txt 

clear 
crontab -l > tasksfile_and_comments.dat
sed '/#/d' tasksfile_and_comments.dat > tasks_only.dat
sed '/#/!d' tasksfile_and_comments.dat > comments_only.dat
sed '/mplayer/!d' tasks_only.dat > mplayer.dat
sed '/mplayer/d' tasks_only.dat > nonmplayer.dat

#Написать скрипт: будильник (в нужный момент проиграть музыку, скрипт запрашивает
#время срабатывания и файл который надо проиграть). Скрипт должен иметь функции:
#создания, редактирования и удаления заданий будильника. Сторонние задания cron, скрипт
#должен игнорировать(не показывать).
exepath=`which mplayer`

echo "введите команду "
echo "0 - вывести список заданий"
echo "1 - удалить задание"
echo "2 - редактировать задание"
echo "3 - добавить задание"

read command
echo "вы ввели $command"

if [[ $command = '0' ]];
then
	TASK_NUM=`grep -c mplayer mplayer.dat`
	if [[ $TASK_NUM -eq 0 ]]
	then 
		echo "нет заданий"
		rm *.dat
		exit 1;
	fi 

	for ((i = 1; i <= $TASK_NUM; i++))
	do 
		t=`cat mplayer.dat | head -n$i | tail -n1`
		echo "$i) $t"
	done

#1 - удалить задание
elif [[ $command = '1' ]];
then
	TASK_NUM=`grep -c mplayer mplayer.dat`

	if [[ $TASK_NUM -eq 0 ]]
	then 
		echo "нет заданий"
		rm *.dat
		exit 1;
	fi 

	for ((i = 1; i <= $TASK_NUM; i++))
	do 
		t=`cat mplayer.dat | head -n$i | tail -n1`
		echo "$i) $t"
	done
	echo "введите номер удаляемого задания "
	read command
	echo "вы ввели $command"

	sed -i "${command}d" mplayer.dat 

	echo "=================="	
	TASK_NUM=`grep -c mplayer mplayer.dat`
	for ((i = 1; i <= $TASK_NUM; i++))
	do 
		t=`cat mplayer.dat | head -n$i | tail -n1`
		echo "$i) $t"
	done

	cat comments_only.dat > tasksfile_and_comments.dat;
	cat nonmplayer.dat >> tasksfile_and_comments.dat
	cat mplayer.dat >> tasksfile_and_comments.dat
	cat "" >> tasksfile_and_comments.dat	
	crontab tasksfile_and_comments.dat
	crontab -l

#2 - редактировать задание
elif [[ $command = '2' ]];
then
	TASK_NUM=`grep -c mplayer mplayer.dat`
	if [[ $TASK_NUM -eq 0 ]]
	then 
		echo "нет заданий"
		rm *.dat
		exit 1;
	fi 
	for ((i = 1; i <= $TASK_NUM; i++))
	do 
		t=`cat mplayer.dat | head -n$i | tail -n1`
		echo "$i) $t"
	done
	echo "введите номер редактируемого задания "
	read command
	echo "вы ввели $command"

	m=`cat mplayer.dat | head -n$command | tail -n1 | awk '{print $1}'`
	echo "сейчас минуты $m, введите новые минуты, "
	read m	

	h=`cat mplayer.dat | head -n$command | tail -n1 | awk '{print $2}'`
	echo "сейчас часы $h, введите новые часы"
	read h
	
	dom=`cat mplayer.dat | head -n$command | tail -n1 | awk '{print $3}'`
	echo "сейчас день недели $dom, введите новый день недели"
	read dom

	mon=`cat mplayer.dat | head -n$command | tail -n1 | awk '{print $4}'`
	echo "сейчас день месяца $mon, введите новый день месяца"
	read mon

	dow=`cat mplayer.dat | head -n$command | tail -n1 | awk '{print $5}'`
	echo "сейчас день недели $dow, введите новый день недели"
	read dow

	dow=`cat mplayer.dat | head -n$command | tail -n1 | awk '{print $7}'`
	echo "введите путь к файлу";
	read filepath
	if [[ $filepath = '*' ]];
	then
		dir=`pwd`
		name=`ls *.mp3`
		filepath="${dir}/${name}"

		echo "Путь к файлу: $filepath"
	fi

	echo "$m $h $dom $mon $dow $exepath $filepath" >> mplayer.dat
	echo "=================="	

	sed -i "${command}d" mplayer.dat 

	cat comments_only.dat > tasksfile_and_comments.dat;
	cat nonmplayer.dat >> tasksfile_and_comments.dat
	cat mplayer.dat >> tasksfile_and_comments.dat
	cat "" >> tasksfile_and_comments.dat
	crontab tasksfile_and_comments.dat
	crontab -l

#3 - добавить задание
elif [[ $command = '3' ]];
then

	echo "добавление";

	echo "введите минуту или *";
	read m
	echo "введите час или *";
	read h
	echo "введите день месяца или *";
	read dom
	echo "введите день месяца или *";
	read mon
	echo "введите день недели или *";
	read dow
	echo "введите путь к файлу с именем или если он лежит в этой же папке, то *";
	read filepath
	if [[ $filepath = '*' ]];
	then
		dir=`pwd`
		name=`ls *.mp3`
		filepath="${dir}/${name}"

		echo "Путь к файлу: $filepath"
	fi

	echo "$m $h $dom $mon $dow $exepath $filepath" >> mplayer.dat
	echo "=================="	

	cat comments_only.dat > tasksfile_and_comments.dat;
	cat nonmplayer.dat >> tasksfile_and_comments.dat
	cat mplayer.dat >> tasksfile_and_comments.dat
	cat "" >> tasksfile_and_comments.dat
	crontab tasksfile_and_comments.dat
	crontab -l

fi

rm *.dat
echo "=================="	

