
#include "utils.h"

#define MAX_UDP_PORTS 2

#define ATOMIC_START() do { DO_OR_DIE(semop(semid, &lock_res, 1), "Lock failed");
#define ATOMIC_END()   DO_OR_DIE(semop(semid, &release_res, 1), "Release failed");} while(0)


static int run_child2_flag = 0;

union semun {
	int val;
	struct semid_ds *buf;
	unsigned short *array;
	struct seminfo *__buf;
};


void child_1_handler()
{
	int cfd_udp;
	const int data_len = 2;
	const int broadcastPermission = 1;
	struct sockaddr_in to_addr;
	int port1 = CLIENT_FIRST_PORT;

	printf("child1 pid %d\n", getpid());
	fflush(stdout);

	while (1) {
		
		printf("child1 udp ");
		fflush(stdout);
		
		cfd_udp = Socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
		Setsockopt(cfd_udp, SOL_SOCKET, SO_BROADCAST, (void *) &broadcastPermission, sizeof(broadcastPermission));
		
		for (int i = 0; i < MAX_UDP_PORTS; i++) {
			
			printf(" %d ", port1 + i);
			fflush(stdout);
			memset(&to_addr, 0, sizeof(struct sockaddr_in)); /* Zero out structure */
			to_addr.sin_family = AF_INET; /* Internet addr family */
			to_addr.sin_addr.s_addr = inet_addr("127.0.0.1"); /* Server IP address */
			to_addr.sin_port = htons(port1 + i); /* Server port */		
			Sendto(cfd_udp, "1\n", data_len, 0, (struct sockaddr*)&to_addr, sizeof(to_addr));

			usleep(200*1000);
		}

		printf("\n");
		fflush(stdout);
		
		Close(cfd_udp);

		sleep(1);
	}
}

void run1(int signum)
{
    printf("SIGINT!\n");
    run_child2_flag = 1;
}

void child_2_handler()
{
	int cfd_udp;
	const int data_len = 2;
    const int broadcastPermission = 1;
	struct sockaddr_in to_addr;
	int port2 = CLIENT_SECOND_PORT;
	
	signal(SIGINT, run1);
	printf("child2 wait signal %d\n", getpid());
	fflush(stdout);
	while (1) {
		if (run_child2_flag) {
			break;
		}
	}


	while (1) {
		
		printf("child1 udp ");
		fflush(stdout);
		
		cfd_udp = Socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
		Setsockopt(cfd_udp, SOL_SOCKET, SO_BROADCAST, (void *) &broadcastPermission, sizeof(broadcastPermission));
		
		for (int i = 0; i < MAX_UDP_PORTS; i++) {
			printf(" %d ", port2 + i);
			fflush(stdout);
			memset(&to_addr, 0, sizeof(struct sockaddr_in)); /* Zero out structure */
			to_addr.sin_family = AF_INET; /* Internet addr family */
			to_addr.sin_addr.s_addr = inet_addr("127.0.0.1"); /* Server IP address */
			to_addr.sin_port = htons(port2 + i); /* Server port */		
			Sendto(cfd_udp, "2\n", data_len, 0, (struct sockaddr*)&to_addr, sizeof(to_addr));

			usleep(200*1000);
		}

		printf("\n");
		fflush(stdout);

		Close(cfd_udp);

		sleep(5);
	}
}


int main(int argc, char* argv[])
{

	char q = 0;
    char s[MAX_MSG_LEN+1] = {0};
    pid_t childpid1, childpid2;
	key_t key;
    int *shm, shmid, semid, len, sfd_tcp, cfd_tcp;
    struct sockaddr_in saddr, caddr;
    union semun arg;
    struct sembuf lock_res = {0, -1, 0};
    struct sembuf release_res = {0, 1, 0};
    //struct sembuf wait_res = {0, 0, 0};


	
    key = Ftok(".", 'l'+2);
    semid = Semget(key, 1, IPC_CREAT | 0666);
    DO_OR_DIE(shmid = shmget(key, sizeof(fifo_t), IPC_CREAT | 0666), "shmget");
    arg.val = 1;
    semctl(semid, 0, SETVAL, arg);

	if ((int*)-1 == (shm = shmat(shmid, NULL, 0))) {
		perror("shmat");
		exit(1);
	}
	memset(shm, 0, sizeof(fifo_t));
	DO_OR_DIE(shmdt(shm), "Ошибка отключения");
	
	printf("serv pid %d\n", getpid());
	fflush(stdout);
	sfd_tcp = Socket(AF_INET, SOCK_STREAM, 0);
	
    /* Set socket to allow broadcast */
	prepare_sockaddr(&saddr, SERVER_PORT);
	Bind(sfd_tcp, (struct sockaddr*)&saddr, sizeof(saddr));
	Listen(sfd_tcp, 1);

    if (0 == (childpid1 = Fork())) {
		child_1_handler();
        return 0;
    }

    if (0 == (childpid2 = Fork())) {
		child_2_handler();
        return 0;
    }
	while (1) {

		len = sizeof(caddr);
		//printf("serv tcp listen on %d\n", SERVER_PORT);				
		cfd_tcp = Accept(sfd_tcp, (struct sockaddr*)&caddr, (socklen_t*)&len);

		if (0 == Fork()) {

			printf("tcp serv child\n");
			fflush(stdout);

			fifo_t* pf;

			ATOMIC_START();
			// Получим доступ к памяти
			if ((int*)-1 == (int*)(pf = shmat(shmid, NULL, 0))) {
				perror("shmat");
				exit(1);
			}

			// Определим кто подключается 
			Read(cfd_tcp, s, 2); // c1 или c2


			if (s[1] == '1') 
			{
				//printf("1a\n");
				uint8_t xbuf[1024] = {0};
				memset(xbuf, 0, 1024);
				msg_t xmmsg = {0};
				memset(&xmmsg, 0, sizeof(msg_t));
				if (WAS_NEVER_FULL == pf->was_fullness) {

					// если не было переполнения, то шлем 
					//printf("1b\n");

					// Команда продолжать 
					q = CONTINUE;
					Write(cfd_tcp, &q, 1);				
					
					// Получаем данные
					size_t msg_len = read_buffer(cfd_tcp, MAX_TCP_LEN, xbuf);
					AMessage* xmsg = NULL;
				    // Распаковываем в структуру 
				    xmsg = amessage__unpack(NULL, msg_len, xbuf);
				    if (xmsg == NULL) {
				        fprintf(stderr, "error unpacking incoming message\n");
				        exit(1);
				    }

				    // Упаковываем для хранения в FIFO 
				
    				xmmsg.msg_len = xmsg->msg_len;
    				xmmsg.sleep_time = xmsg->sleep_time;
					for (int i = 0; i < xmsg->msg_len; i++)
					{
						xmmsg.msg[i] = (char)xmsg->msg[i];
					}
					
					printf("serv1 tcp <-- { %d %d %s }\n", 
											xmmsg.sleep_time, 
											xmmsg.msg_len, 
											xmmsg.msg);
					fflush(stdout);

					// Сохраняем в fifo 
					fifo_put(pf, &xmmsg);
				    // Free the unpacked message
				    amessage__free_unpacked(xmsg, NULL);

					if (RING_SIZE == fifo_get_size(pf)) {
						if (0 == pf->kill_child1) {
							pf->kill_child1 = 1;
							
							printf("buffer fulllllllllllllllllllllllllllllll\n");
							printf("child 1 -------------------------- dead\n");	
							fflush(stdout);				
							pf->was_fullness = WAS_FULL;
							kill(childpid1, SIGKILL);
						}
					}
				} else {
					// Один раз уже заполняли 
					// Умирай
					q = DIE;
					Write(cfd_tcp, &q, 1);				
					printf("COMMAND 1 TO DIE \n");
					fflush(stdout);
				}

				print_fifo(pf);
			} 
			else if (s[1] == '2')
			{
				printf("2\n");
				void* zbuf;
				msg_t zmmsg = {0};
				size_t zlen;
				// Если буффер не пуст 
				if (fifo_get_size(pf)) {
					q = CONTINUE;
					Write(cfd_tcp, &q, 1);	 

					fifo_get(pf, &zmmsg);

					AMessage zmsg = AMESSAGE__INIT;
					zmsg.sleep_time = zmmsg.sleep_time;
					zmsg.msg_len = zmmsg.msg_len;
					zmsg.n_msg = zmsg.msg_len;
					if (NULL == (zmsg.msg = malloc(sizeof(int) * zmsg.n_msg))) {
						perror("2 malloc\n");
						return -1;
					}
					
					
					printf("%s", "<");
					printf("%d %d %d ", zmsg.sleep_time, zmsg.msg_len, (int)zmsg.n_msg);
					for (int i = 0; i < zmsg.n_msg; i++) {
						zmsg.msg[i] = zmmsg.msg[i];
						printf("%c", zmsg.msg[i]);
					}
					printf("%s\n", ">");

				    zlen = amessage__get_packed_size(&zmsg);
				    zbuf = malloc(zlen);
				    amessage__pack(&zmsg, zbuf);

					Write(cfd_tcp, zbuf, zlen);

					free(zmsg.msg);
					free(zbuf);					
					//printf("serv2 tcp --> {%d %d %s}\n", zmsg.sleep_time, zmsg.msg_len, zmsg.msg);
					//fflush(stdout);
				} else {

					// отправлять нечего 
					q = DIE;
					Write(cfd_tcp, &q, 1);	
					pf->num_killed_udp++;

					printf("fifo buffer empty, killed udp cnt %d\n", pf->num_killed_udp);
					

					// Если очередь пуста
					if (1 == pf->first_connect){
						if (WAS_FULL == pf->was_fullness) {
							if (!fifo_get_size(pf)) {
								if (pf->num_killed_udp >= MAX_UDP_PORTS) 
								{
									kill(childpid2, SIGKILL);
									kill(childpid1, SIGKILL);
									
									printf("child 1 -------------------------- dead\n");
									fflush(stdout);
									printf("child 2 -------------------------- dead\n");
									fflush(stdout);

								    DO_OR_DIE(shmdt(shm), "Ошибка отключения");
								    DO_OR_DIE(shmctl(shmid, IPC_RMID, 0), "Невозможно удалить область\n");
								    DO_OR_DIE(semctl(semid, 0, IPC_RMID), "Невозможно удалить семафот\n");
									printf("server exit\n");
									fflush(stdout);
									kill(getppid(), SIGKILL);
								}
							}
						}
					}
				}
								
				print_fifo(pf);
			} else {
				printf("who?\n");
				fflush(stdout);
			}
			if (0 == pf->first_connect) {
				pf->first_connect = 1;
				kill(childpid2, SIGINT);
			}

			DO_OR_DIE(shmdt(pf), "Ошибка отключения");
			ATOMIC_END();
			

			Close(cfd_tcp);
			return 0;
		} // endfork 
		Close(cfd_tcp);


		//break;
	} // endwhile 
	kill(childpid2, SIGKILL);
	kill(childpid1, SIGKILL);
    DO_OR_DIE(shmdt(shm), "Ошибка отключения");
    DO_OR_DIE(shmctl(shmid, IPC_RMID, 0), "Невозможно удалить область\n");
    DO_OR_DIE(semctl(semid, 0, IPC_RMID), "Невозможно удалить семафот\n");
	printf("server exit\n");
	fflush(stdout);
    return 0;
}
