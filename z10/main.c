#include <stdio.h>
#include <signal.h>
#include <sys/wait.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/errno.h>
#include <sys/sem.h>
#include <errno.h>
#include <pthread.h>

// Поиск указанной строки в указанном файле. Обработка одной строки в порожденном процессе.

#define ATOMIC(mutex, x)             \
    do {                             \
        pthread_mutex_lock(mutex);   \
        x;                           \
        pthread_mutex_unlock(mutex); \
    } while (0)

#define MAXLEN 1024

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

char* instring = NULL;
int num_instrings = 0;
char** mas = NULL;
pthread_t* thread_lines;

typedef struct {
    char* p;
    int num;
} shared_t;

void* search_func(void* arg)
{

    char* p = NULL;
    shared_t shared;

    ATOMIC(&mutex,

		memcpy(&shared, arg, sizeof(shared_t));

		if (NULL == (p = strstr(shared.p, instring))) {
			//printf ("Не Найдено\n");
		} else {
			printf ("Найдено строка %d: %s\n", shared.num+1, shared.p);
			num_instrings++; 
		}
	);

    pthread_exit(NULL);
}

int main(int argc, char** argv)
{
    FILE* in = NULL;
    int lines_count = 1;
    int result = 0;

    char c = 0;
    char buffer[MAXLEN] = { 0 };

    if (argc < 3) {
        fprintf(stderr, "Мало аргументов. Используйте <имя файла> <искомый текст>\n");
        exit(1);
    }

    instring = malloc(strlen(argv[2]) + 1);
    memset(instring, 0, strlen(argv[2]) + 1);
    strcat(instring, argv[2]);

    // Открываем файл
    if (NULL == (in = fopen(argv[1], "r"))) {
        printf("error open\n");
        exit(0);
    }
    // Количество строк
    while (EOF != (c = fgetc(in))) {
        if ('\n' == c) {
            lines_count++;
        }
    }
    printf("в файле %d строк\n", lines_count);

    // Устанавливаем на начало файла
    if (fseek(in, 0, SEEK_SET) < 0) {
        printf("error lseek\n");
        exit(0);
    }

    thread_lines = malloc(lines_count * sizeof(pthread_t));
    mas = (char**)malloc(lines_count * sizeof(char*));
    for (int i = 0; i < lines_count; i++) {
        fgets(buffer, MAXLEN, in);
        mas[i] = (char*)malloc(sizeof(char) * strlen(buffer)); //выделяем память для строки
        strcpy(mas[i], buffer); //копируем строку из буфера в массив указателей
    }

    fclose(in);

    shared_t* shared = malloc(lines_count * sizeof(shared_t));
    for (int i = 0; i < lines_count; i++) {

        shared[i].p = mas[i];
        shared[i].num = i;
    }

    for (int i = 0; i < lines_count; i++) {

        result = pthread_create(&thread_lines[i], NULL, search_func, &shared[i]);
        if (result != 0) {
            perror("Creating thread error");
            pthread_exit(NULL);
        }
    }

    for (int i = 0; i < lines_count; i++) {
        pthread_join(thread_lines[i], NULL);
    }

    printf("\n========================\n");
    printf("Конец, найдено %d подстрок\n", num_instrings);

    pthread_exit(NULL);

    for (int i = 0; i < lines_count; i++) {
        free(mas[i]); // освобождаем память для отдельной строки
    }
    free(mas); // освобождаем памать для массива указателей на строки
    free(shared);

    exit(0);
}
