
// Если клиент 1-го типа получает этот пакет, то отправляет по TCP сообщение (время T обработки сообщения,
// длина случайной строки, случайная строка) . После отправки сообщения клиент засыпает на время T. Строка
// случайной длины и время T - генерируется случайно, максимальная длина строки и интервал времени задается
// через константы.

#include "utils.h"


static int second_port = 5555;

int main(int argc, char* argv[])
{
    int cfd_udp, cfd_tcp;
    struct sockaddr_in my_addr, from_addr;
    unsigned int from_addr_len;
    union {
        msg_t message;
        char buffer[sizeof(msg_t)];
    } u;
    char s[MAX_MSG_LEN+1] = {0};

    from_addr_len = sizeof(from_addr);

    cfd_udp = Socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);

	second_port = atoi(argv[1]);

	memset(&my_addr, 0, sizeof(struct sockaddr_in)); /* Zero out structure */
	my_addr.sin_family = AF_INET; /* Internet addr family */
	my_addr.sin_addr.s_addr = htonl(INADDR_ANY); /* Server IP address */
	my_addr.sin_port = htons(second_port); /* Server port */
		
	if (bind(cfd_udp, (struct sockaddr*)&my_addr, sizeof(my_addr)) < 0) {
        perror("bind\n");
        return -1;
	}

    //flags = Fcntl(cfd_udp, F_GETFL, 0);
    //Fcntl(cfd_udp, F_SETFL, flags | O_NONBLOCK);

    printf("2 client listen, %d\n", second_port);
    fflush(stdout);

    memset(u.buffer, 0, sizeof(u.buffer));
    while (1) {

		Recvfrom(cfd_udp, s, 1, 0, (struct sockaddr*)&from_addr, &from_addr_len);

        //printf("client2 --> %s\n", s);

        cfd_tcp = Socket(AF_INET, SOCK_STREAM, 0);
        prepare_sockaddr(&from_addr, SERVER_PORT);

        Connect(cfd_tcp, (struct sockaddr*)&from_addr, sizeof(from_addr));

        Write(cfd_tcp, "c2", 2);
        Read(cfd_tcp, s, 1);
        if (*s == DIE) {
            printf("client2 can't rx\n");
            Close(cfd_tcp);
            Close(cfd_udp);
            return -2;
        }
        
        Read(cfd_tcp, u.buffer, sizeof(u.buffer));
        printf("client2 rx: %d %d %s\n", u.message.sleep_time, u.message.msg_len, u.message.msg);

        Close(cfd_tcp);
        sleep(u.message.sleep_time);
		//printf("client udp wait\n");

    }

    Close(cfd_udp);

    printf("client dead\n");

    return 0;
}
