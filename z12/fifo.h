#ifndef __FIFO_H
#define __FIFO_H

//size: 4,8,16,32...128, 256 ...
#define FIFO( size, one_size_t )\
  struct {\
    one_size_t buf[size];\
    volatile uint16_t tail;\
    volatile uint16_t head;\
  } 

/**
  * @brief	The number of elements in the queue
  */
#define FIFO_COUNT(fifo)     (fifo.head-fifo.tail)

/**
  * @brief FIFO size
  */
#define FIFO_SIZE(fifo)      ( sizeof(fifo.buf)/sizeof(fifo.buf[0]) )

/**
  * @brief
  */
#define FIFO_IS_FULL(fifo)   (FIFO_COUNT(fifo) == FIFO_SIZE(fifo))

/**
  * @brief
  */
#define FIFO_IS_EMPTY(fifo)  (fifo.tail == fifo.head)

/**
  * @brief The amount of free space
  */
#define FIFO_SPACE(fifo)     (FIFO_SIZE(fifo)-FIFO_COUNT(fifo))

/**
  * @brief Insert an element
  */
#define FIFO_PUT(fifo, byte) \
  {\
    fifo.buf[fifo.head & (FIFO_SIZE(fifo)-1)]=byte;\
    fifo.head++;\
  }

/**
  * @brief Get an element
  */
#define FIFO_GET(fifo) (fifo.buf[fifo.tail & (FIFO_SIZE(fifo)-1)])

/**
  * @brief
  */
#define FIFO_DROP(fifo)   \
  {\
      fifo.tail++; \
  }

/**
  * @brief
  */
#define FIFO_FLUSH(fifo)   \
  {\
    fifo.tail=0;\
    fifo.head=0;\
  } 

#endif //__FIFO_H
