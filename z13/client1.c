
// Если клиент 1-го типа получает этот пакет, то отправляет по TCP сообщение (время T обработки сообщения,
// длина случайной строки, случайная строка) . После отправки сообщения клиент засыпает на время T. Строка
// случайной длины и время T - генерируется случайно, максимальная длина строки и интервал времени задается
// через константы.

#include "utils.h"
#include <time.h>


static int first_port = 4444;

static void msg_create(AMessage* msg)
{
    srand(time(NULL) % 50 + getpid() + first_port%5);
    //memset(msg, 0, sizeof(AMessage));   
    msg->sleep_time = 1 + rand() % 3;    
    msg->n_msg = msg->msg_len = 1 + rand() % MAX_MSG_LEN;
    msg->msg = malloc(sizeof(int) * msg->n_msg);
    memset(msg->msg, 0, (sizeof(int) * msg->n_msg));
    for (int i = 0; i < msg->n_msg; i++) {
        msg->msg[i] = (int)('a' + rand() % 26);
    }
}

int main(int argc, char* argv[])
{
    int cfd_udp, cfd_tcp;
    struct sockaddr_in my_addr, from_addr;
    unsigned int from_addr_len = 0;
    char s[MAX_MSG_LEN+1] = {0};
    AMessage xmsg = AMESSAGE__INIT;
    void* buf = NULL;
    unsigned len = 0;


    from_addr_len = sizeof(from_addr);
    cfd_udp = Socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    
    if (argc != 2) { // Allow one or two integers
        fprintf(stderr, "use port\n");
        return 1;
    }

    first_port = atoi(argv[1]);

    memset(&my_addr, 0, sizeof(struct sockaddr_in)); // Zero out structure 
    my_addr.sin_family = AF_INET; // Internet addr family 
    my_addr.sin_addr.s_addr = htonl(INADDR_ANY); // Server IP address 
    my_addr.sin_port = htons(first_port); // Server port 
        
    if (bind(cfd_udp, (struct sockaddr*)&my_addr, sizeof(my_addr)) < 0) {
        perror("bind\n");
        return -1;
    }

    //flags = Fcntl(cfd_udp, F_GETFL, 0);
    //Fcntl(cfd_udp, F_SETFL, flags | O_NONBLOCK);

    printf("1 client listen, %d\n", first_port);
    fflush(stdout);

    
    while (1) {

        Recvfrom(cfd_udp, s, 1, 0, (struct sockaddr*)&from_addr, &from_addr_len);


        cfd_tcp = Socket(AF_INET, SOCK_STREAM, 0);
        prepare_sockaddr(&from_addr, SERVER_PORT);

        Connect(cfd_tcp, (struct sockaddr*)&from_addr, sizeof(from_addr));

        // Кто мы 
        Write(cfd_tcp, "c1", 2);
        // Что нам делать 
        Read(cfd_tcp, s, 1);
        if (*s == DIE) {
            printf("client1 can't rx\n");
            Close(cfd_tcp);
            Close(cfd_udp);
            return -2;
        }

        // Генерируем данные
        msg_create(&xmsg);

        len = amessage__get_packed_size(&xmsg);
        buf = malloc(len);
        amessage__pack(&xmsg, buf);

        printf("client1 generate: %d %d %s \n", xmsg.sleep_time, xmsg.msg_len, (char*)buf);
        fflush(stdout);

        Write(cfd_tcp, buf, len);

        free(xmsg.msg);
        free(buf);
        Close(cfd_tcp);

        sleep(xmsg.sleep_time);
    }

    Close(cfd_udp);

    printf("client dead\n");
    fflush(stdout);

    return 0;
}

