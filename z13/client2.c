
// Если клиент 1-го типа получает этот пакет, то отправляет по TCP сообщение (время T обработки сообщения,
// длина случайной строки, случайная строка) . После отправки сообщения клиент засыпает на время T. Строка
// случайной длины и время T - генерируется случайно, максимальная длина строки и интервал времени задается
// через константы.

#include "utils.h"


static int second_port = 5555;


int main(int argc, char* argv[])
{
    int cfd_udp, cfd_tcp;
    struct sockaddr_in my_addr, from_addr;
    unsigned int from_addr_len;
    char s[MAX_MSG_LEN+1] = {0};
    uint8_t xbuf[MAX_TCP_LEN] = {0};
    AMessage* msg = NULL;
    size_t msg_len;

    from_addr_len = sizeof(from_addr);
    cfd_udp = Socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    
    if (argc != 2) { // Allow one or two integers
        fprintf(stderr, "use port\n");
        return 1;
    }

	second_port = atoi(argv[1]);

	memset(&my_addr, 0, sizeof(struct sockaddr_in)); /* Zero out structure */
	my_addr.sin_family = AF_INET; /* Internet addr family */
	my_addr.sin_addr.s_addr = htonl(INADDR_ANY); /* Server IP address */
	my_addr.sin_port = htons(second_port); /* Server port */
		
	if (bind(cfd_udp, (struct sockaddr*)&my_addr, sizeof(my_addr)) < 0) {
        perror("bind\n");
        return -1;
	}

    printf("2 client listen, %d\n", second_port);
    fflush(stdout);



    while (1) {

        memset(xbuf, 0, MAX_TCP_LEN);
		Recvfrom(cfd_udp, s, 1, 0, (struct sockaddr*)&from_addr, &from_addr_len);

        //printf("client2 --> %s\n", s);

        cfd_tcp = Socket(AF_INET, SOCK_STREAM, 0);
        prepare_sockaddr(&from_addr, SERVER_PORT);

        Connect(cfd_tcp, (struct sockaddr*)&from_addr, sizeof(from_addr));

        // Кто мы 
        Write(cfd_tcp, "c2", 2);
        // Что нам делать 
        Read(cfd_tcp, s, 1);
        if (*s == DIE) {
            printf("client2 can't rx\n");
            Close(cfd_tcp);
            Close(cfd_udp);
            return -2;
        }

        // Читаем данные
        msg_len = read_buffer(cfd_tcp, MAX_TCP_LEN, xbuf);
        
        // Пакет в структуру 
        msg = amessage__unpack(NULL, msg_len, xbuf);
        if (msg == NULL) {
            fprintf(stderr, "error unpacking incoming message\n");
            exit(1);
        }

        // Выводим что получили
        printf("client2 rx: %d %d ", msg->sleep_time, msg->msg_len);
        for (int i = 0; i < msg->n_msg; i++) {
            printf ("%c", (char)msg->msg[i]);
        }
        printf ("\n");
        Close(cfd_tcp);
        sleep(msg->sleep_time);
        amessage__free_unpacked(msg, NULL);
		//printf("client udp wait\n");

    }

    Close(cfd_udp);

    printf("client dead\n");

    return 0;
}
