
#ifndef	__utils_h
#define	__utils_h

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <wait.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h> 
#include <sys/shm.h>
#include <sys/sem.h>
#include "amessage.pb-c.h"


#define DO_OR_DIE(x, s) \
    do {                \
        if ((x) < 0) {  \
            perror(s);  \
            exit(1);    \
        }               \
    \
} while (0)
	
	
#define MAX_MSG_LEN 10
#define MAX_TCP_LEN 50
	
typedef struct {
    char msg_len;
    char sleep_time;
    char msg[MAX_MSG_LEN];
} __attribute__((packed)) msg_t;

enum {
	FROM_S = 0,
	FROM_C1,
	FROM_C2,	
};


enum {
	CLIENT_FIRST_PORT = 4444,
	CLIENT_SECOND_PORT = 5555,
	SERVER_PORT = 6666,
};

enum {
	CONTINUE = 1, 
	DIE = 2,
};

enum {
	WAS_NEVER_FULL = 0, 
	WAS_FULL = 1,
};

#define RING_SIZE 4
typedef struct {
	int num_killed_udp;
	int was_fullness;
	int kill_child1;
	int first_connect;
	int cnt;
    int tail;
	int head;
    msg_t msgs[RING_SIZE];
}  fifo_t;

size_t read_buffer(int desc, unsigned max_length, uint8_t* out);

void fifo_get(fifo_t* pfifo, msg_t* msg);
int fifo_get_size(fifo_t* pfifo);
int fifo_put(fifo_t* pfifo, msg_t* msg);
void print_fifo(fifo_t* pfifo);


int Semctl(int id, int semnum, int cmd, ...);
int Shmget(key_t key, size_t size, int flags);
int Semget(key_t key, int nsems, int flag);
key_t Ftok(const char *pathname, int id);
pid_t Fork(void);
void Close(int fd);
void Listen(int fd, int backlog);
int Fcntl(int fd, int cmd, int arg);
void Write(int fd, void* ptr, size_t nbytes);
ssize_t Read(int fd, void* ptr, size_t nbytes);
int Socket(int family, int type, int protocol);
void Bind(int fd, const struct sockaddr* sa, socklen_t salen);
int Accept(int fd, struct sockaddr* sa, socklen_t* salenptr);
void Connect(int fd, const struct sockaddr* sa, socklen_t salen);
void Setsockopt(int fd, int level, int optname, const void *optval, socklen_t optlen);
void Sendto(int fd, const void* ptr, size_t nbytes, int flags, const struct sockaddr* sa, socklen_t salen);
ssize_t Recvfrom(int fd, void* ptr, size_t nbytes, int flags, struct sockaddr* sa, socklen_t* salenptr);

void msg_buf_to_msg(msg_t* msg, char* p);
void msg_msg_to_buf(char* p, msg_t* msg);

void prepare_sockaddr(struct sockaddr_in* addr, int port);



#endif	/* __utils_h */
