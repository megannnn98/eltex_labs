#include "TCPEchoServer.h"  /* TCP echo server includes */
#include "utils.h"
#include <stdio.h>
#include <signal.h>
#include <sys/wait.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/errno.h>
#include <sys/sem.h>
#include <errno.h>
#include <pthread.h>
#include <stdbool.h>

int max_udp_ports = 1;
#define RCVBUFSIZE 64   /* Size of receive buffer */

#define ATOMIC(mutex, x) do {pthread_mutex_lock(mutex);x;pthread_mutex_unlock(mutex);}while(0)

static pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
static fifo_t fifo = {0};


void *ThreadMain(void *arg);            /* Main program of a thread */


/* Structure of arguments to pass to client thread */
struct ThreadArgs
{
    int clntSock;                      /* Socket descriptor for client */
};


void send_udp(int t, int cnt, int port)
{
	int cfd;
	struct sockaddr_in to_addr;

    printf("child%d udp <cnt %d> {", t, cnt);
    fflush(stdout);

    cfd = Socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);

    printf(" %d ", port);
    fflush(stdout);
    memset(&to_addr, 0, sizeof(struct sockaddr_in)); // Zero out structure 
    to_addr.sin_family = AF_INET; // Internet addr family 
    to_addr.sin_addr.s_addr = inet_addr("127.0.0.1"); // Server IP address 
    to_addr.sin_port = htons(port); // Server port         
    Sendto(cfd, "", 0, 0, (struct sockaddr*)&to_addr, sizeof(to_addr));

    usleep(200*1000);

    Close(cfd);

    printf("} \n");
    fflush(stdout);
}


void* child_1_handler(void* arg)
{
    const int data_len = 2;
    struct sockaddr_in to_addr;
    int port1 = CLIENT_FIRST_PORT;
    int fifo_local_cnt = 0;

    printf("child1 run\n");


    while (1) {

        for (int i = 0; i < max_udp_ports; i++) {

            ATOMIC(
                &mutex,
                fifo_local_cnt = fifo_get_size(&fifo);
            );

            if (RING_SIZE == fifo_local_cnt) {
                break;
            }

            send_udp(1, fifo_local_cnt, port1 + i);
            sleep(1);
        }        

        
    }
    pthread_exit(NULL);
}


void* child_2_handler(void* arg)
{
    const int data_len = 2;
    struct sockaddr_in to_addr;
    int port2 = CLIENT_SECOND_PORT;
    int fifo_local_cnt = 0;

    printf("child2 run\n");


    while (1) {

        for (int i = 0; i < max_udp_ports; i++) {

            ATOMIC(
                &mutex,
                fifo_local_cnt = fifo_get_size(&fifo);
            );

            if (0 == fifo_local_cnt) {
                break;
            }

            send_udp(2, fifo_local_cnt, port2 + i);

            sleep(4);
        }        

        
    }
    pthread_exit(NULL);
}

void *ThreadMain(void *threadArgs)
{
    char s[RCVBUFSIZE];        /* Buffer for echo string */
    int msg_len;                    /* Size of received message */
    int clntSock;                   /* Socket descriptor for client connection */

    /* Guarantees that thread resources are deallocated upon return */
    pthread_detach(pthread_self()); 

    /* Extract socket file descriptor from argument */
    clntSock = ((struct ThreadArgs *)threadArgs)->clntSock;
    free(threadArgs);              /* Deallocate memory for argument */

    /* Receive message from client */
    if ((msg_len = recv(clntSock, s, RCVBUFSIZE, 0)) < 0) {
        DieWithError("recv() failed");
    }


    if (s[1] == '1') {

        msg_t xmmsg = {0};
        memset(s, 0, RCVBUFSIZE);
        /* Receive message from client */
        if ((msg_len = recv(clntSock, s, RCVBUFSIZE, 0)) < 0) {
            DieWithError("recv() failed");
        }
        AMessage* xmsg = NULL;
        // Распаковываем в структуру 
        xmsg = amessage__unpack(NULL, msg_len, s);
        if (xmsg == NULL) {
            fprintf(stderr, "error unpacking incoming message\n");
            exit(1);
        }

        // Упаковываем для хранения в FIFO 
    
        xmmsg.msg_len = xmsg->msg_len;
        xmmsg.sleep_time = xmsg->sleep_time;
        for (int i = 0; i < xmsg->msg_len; i++)
        {
            xmmsg.msg[i] = (char)xmsg->msg[i];
        }
        
        printf("serv1 tcp <-- { %d %d %s }\n", 
                                xmmsg.sleep_time, 
                                xmmsg.msg_len, 
                                xmmsg.msg);
        fflush(stdout);
        
        ATOMIC(
            &mutex, 
            // Сохраняем в fifo 
            fifo_put(&fifo, &xmmsg);
            // Free the unpacked message
            amessage__free_unpacked(xmsg, NULL);
            print_fifo(&fifo);
        );

    } else if (s[1] == '2') {
        void* zbuf;
        msg_t zmmsg = {0};
        size_t zlen;


        ATOMIC(
            &mutex, 
            fifo_get(&fifo, &zmmsg);
        );

        AMessage zmsg = AMESSAGE__INIT;
        zmsg.sleep_time = zmmsg.sleep_time;
        zmsg.msg_len = zmmsg.msg_len;
        zmsg.n_msg = zmsg.msg_len;
        if (NULL == (zmsg.msg = malloc(sizeof(int) * zmsg.n_msg))) {
            perror("2 malloc\n");
            pthread_exit(NULL);
        }
        
        
        printf("%s", "<");
        printf("%d %d %d ", zmsg.sleep_time, zmsg.msg_len, (int)zmsg.n_msg);
        for (int i = 0; i < zmsg.n_msg; i++) {
            zmsg.msg[i] = zmmsg.msg[i];
            printf("%c", zmsg.msg[i]);
        }
        printf("%s\n", ">");

        zlen = amessage__get_packed_size(&zmsg);
        zbuf = malloc(zlen);
        amessage__pack(&zmsg, zbuf);

        Write(clntSock, zbuf, zlen);

        free(zmsg.msg);
        free(zbuf);                 
        //printf("serv2 tcp --> {%d %d %s}\n", zmsg.sleep_time, zmsg.msg_len, zmsg.msg);
        //fflush(stdout);
        print_fifo(&fifo);

    } else {
        printf("who?\n");
        fflush(stdout);
    }

    close(clntSock);    /* Close client socket */

    pthread_exit(NULL);
}



int main(int argc, char *argv[])
{
    int result;
    int servSock;                    /* Socket descriptor for server */
    int clntSock;                    /* Socket descriptor for client */
    unsigned short echoServPort;     /* Server port */
    pthread_t threadID, thread_child1, thread_child2; /* Thread ID from pthread_create() */
    struct ThreadArgs *threadArgs;   /* Pointer to argument structure for thread */

    if (argc != 3)     /* Test for correct number of arguments */
    {
        fprintf(stderr,"Usage:  %s <SERVER PORT> <num udps>\n", argv[0]);
        exit(1);
    }
	
	max_udp_ports = atoi(argv[2]);

    result = pthread_create(&thread_child1, NULL, child_1_handler, NULL);
    if (result != 0) {
        perror("Creating the child1 thread");
        return EXIT_FAILURE;
    }
    result = pthread_create(&thread_child2, NULL, child_2_handler, NULL);
    if (result != 0) {
        perror("Creating the child1 thread");
        return EXIT_FAILURE;
    }

    echoServPort = atoi(argv[1]);  /* First arg:  local port */
    servSock = CreateTCPServerSocket(echoServPort);

    for (;;) /* run forever */
    {
		clntSock = AcceptTCPConnection(servSock);

        /* Create separate memory for client argument */
        if (NULL == (threadArgs = (struct ThreadArgs *) malloc(sizeof(struct ThreadArgs)))) {
            DieWithError("malloc() failed");
        }

        threadArgs->clntSock = clntSock;

        /* Create client thread */
        if (pthread_create(&threadID, NULL, ThreadMain, (void *) threadArgs) != 0) {
            DieWithError("pthread_create() failed");
        }
        printf("with thread %ld\n", (long int) threadID);
    }

    /* NOT REACHED */
}

