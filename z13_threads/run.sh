#/bin/bash

echo $1

p1=4444;
p2=5555;
sx1=0;

xterm -geometry 50x80+0+0 -e ./server 6666 $1 &

sleep 1

for (( k=0; k < $1; k++ ))
do 
	xterm -geometry 30x40+$sx1+0 -e ./client1 $p1 &
	xterm -geometry 30x40+$sx1+300 -e ./client2 $p2 &
	let "p1 += 1"
	let "p2 += 1"
	let "sx1 += 100"

	sleep 1
done

# a=2334 # Целое число.
# let "a += 1"
# echo "a = $a " # a = 2335
# echo # Все еще целое число.
# b=${a/23/BB} # замена "23" на "BB".
# Происходит трансформация числа в строку.