
#include "utils.h"


void prepare_sockaddr(struct sockaddr_in* addr, int port)
{
    memset(addr, 0, sizeof(struct sockaddr_in)); /* Zero out structure */
    addr->sin_family = AF_INET; /* Internet addr family */
    addr->sin_addr.s_addr = inet_addr("127.0.0.1"); /* Server IP address */
    addr->sin_port = htons(port); /* Server port */
}
/*
void msg_buf_to_msg(AMessage* msg, char* p)
{
    memset(msg, 0, sizeof(AMessage));
    msg->sleep_time = p[0] - '0';
    msg->msg_len = p[1] - '0';
    memcpy(msg->msg, &p[2], msg->msg_len);
}
void msg_AMessageo_buf(char* p, AMessage* msg)
{
    memset(p, 0, sizeof(AMessage));
	sprintf(p, "%d%d%s", msg->sleep_time, msg->msg_len, msg->msg);
}*/
size_t
read_buffer(int desc, unsigned max_length, uint8_t* out)
{
    size_t cur_len = 0;
    size_t nread;
    while ((nread = Read(desc, out + cur_len, 1)) != 0) {

        //printf("1d\n");
        cur_len += nread;
        if (cur_len == max_length) {
            fprintf(stderr, "max message length exceeded\n");
            exit(1);
        }
    }
    return cur_len;
}


int fifo_put(fifo_t* pfifo, msg_t* msg)
{
    if (pfifo->cnt < RING_SIZE ) {

        memset(&pfifo->msgs[pfifo->tail], 0, sizeof(msg_t));
        pfifo->msgs[pfifo->tail].sleep_time = msg->sleep_time;
        pfifo->msgs[pfifo->tail].msg_len = msg->msg_len;
        memcpy(pfifo->msgs[pfifo->tail].msg, msg->msg, msg->msg_len);
        pfifo->cnt++;
        pfifo->tail++;
        if (RING_SIZE == pfifo->tail) {
            pfifo->tail = 0;
        }
        return 0;
    }
    return -1;
}
void fifo_get(fifo_t* pfifo, msg_t* msg)
{
    if (pfifo->cnt > 0) {
        memset(msg, 0, sizeof(msg_t));
        msg->msg_len = pfifo->msgs[pfifo->head].msg_len;
        msg->sleep_time = pfifo->msgs[pfifo->head].sleep_time;
        strcat(msg->msg, pfifo->msgs[pfifo->head].msg);
        pfifo->cnt--;
        pfifo->head++;
        if (RING_SIZE == pfifo->head) {
            pfifo->head = 0;
        }
    }
}

void print_fifo(fifo_t* pfifo)
{
    char s[MAX_MSG_LEN+1] = {0};
    printf(">> head=%d tail=%d capacity %d\n", pfifo->tail, pfifo->head, fifo_get_size(pfifo));
    fflush(stdout);
    for (int i = 0; i < RING_SIZE; i++) {
        memset(s, 0, MAX_MSG_LEN);
        memcpy(s, pfifo->msgs[i].msg, pfifo->msgs[i].msg_len);
        memset(pfifo->msgs[i].msg, 0, pfifo->msgs[i].msg_len+1);
        memcpy(pfifo->msgs[i].msg, s, pfifo->msgs[i].msg_len);
        printf(">> [%d] %d %d %s\n", i, pfifo->msgs[i].sleep_time, pfifo->msgs[i].msg_len, pfifo->msgs[i].msg);
        fflush(stdout);
    }
}
int fifo_get_size(fifo_t* pfifo)
{
    return pfifo->cnt;    
}




int
Shmget(key_t key, size_t size, int flags)
{
    int     rc;

    if ( (rc = shmget(key, size, flags)) == -1) {
        perror("shmget error");
        exit(1);
    }
    return(rc);
}

int
Semget(key_t key, int nsems, int flag)
{
    int     rc;

    if ( (rc = semget(key, nsems, flag)) == -1){
        perror("semget error");
        exit(1);
    }
    return(rc);
}

key_t
Ftok(const char *pathname, int id)
{
    key_t   key;

    if ( (key = ftok(pathname, id)) == -1) {
        perror("ftok error for pathname.. and id..");
        exit(1);
    }
    return(key);
}

void
Setsockopt(int fd, int level, int optname, const void *optval, socklen_t optlen)
{
	if (setsockopt(fd, level, optname, optval, optlen) < 0) {
        perror("setsockopt error");
        exit(1);
    }
}

int Socket(int family, int type, int protocol)
{
    int n;

    if ((n = socket(family, type, protocol)) < 0) {
        perror("socket error");
        exit(1);
    }
    return (n);
}

ssize_t
Read(int fd, void* ptr, size_t nbytes)
{
    ssize_t n;

    if ((n = read(fd, ptr, nbytes)) == -1) {
        perror("read error");
        exit(1);
    }
    return (n);
}
int Accept(int fd, struct sockaddr* sa, socklen_t* salenptr)
{
    int n;

again:
    if ((n = accept(fd, sa, salenptr)) < 0) {
#ifdef EPROTO
        if (errno == EPROTO || errno == ECONNABORTED)
#else
        if (errno == ECONNABORTED)
#endif
            goto again;
        else {
            perror("accept error");
            exit(1);
        }
    }
    return (n);
}
void Sendto(int fd, const void* ptr, size_t nbytes, int flags,
    const struct sockaddr* sa, socklen_t salen)
{
    if (sendto(fd, ptr, nbytes, flags, sa, salen) != (ssize_t)nbytes) {
        perror("sendto error");
        exit(1);
    }
}
void Listen(int fd, int backlog)
{
    char* ptr;

    /*4can override 2nd argument with environment variable */
    if ((ptr = getenv("LISTENQ")) != NULL)
        backlog = atoi(ptr);

    if (listen(fd, backlog) < 0) {
        perror("listen error");
        exit(1);
    }
}
pid_t Fork(void)
{
    pid_t pid;

    if ((pid = fork()) == -1) {
        perror("fork error");
        exit(1);
    }
    return (pid);
}

void Connect(int fd, const struct sockaddr* sa, socklen_t salen)
{
    if (connect(fd, sa, salen) < 0) {
        perror("connect error");
        exit(1);
    }
}
void Bind(int fd, const struct sockaddr* sa, socklen_t salen)
{
    if (bind(fd, sa, salen) < 0) {
        perror("bind error");
        exit(1);
    }
}
void Close(int fd)
{
    if (close(fd) == -1) {
        perror("close error");
        exit(1);
    }
}
void Write(int fd, void* ptr, size_t nbytes)
{
    if (write(fd, ptr, nbytes) != nbytes) {
        perror("write error");
        exit(1);
    }
}
ssize_t
Recvfrom(int fd, void* ptr, size_t nbytes, int flags,
    struct sockaddr* sa, socklen_t* salenptr)
{
    ssize_t n;

    if ((n = recvfrom(fd, ptr, nbytes, flags, sa, salenptr)) < 0) {
        perror("recvfrom error");
        exit(1);
    }
    return (n);
}
int Fcntl(int fd, int cmd, int arg)
{
    int n;

    if ((n = fcntl(fd, cmd, arg)) == -1) {
        perror("fcntl error");
        exit(1);
    }
    return (n);
}