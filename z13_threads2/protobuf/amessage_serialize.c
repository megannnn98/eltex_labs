#include <stdio.h>
#include <stdlib.h>
#include "amessage.pb-c.h"


static void msg_create(AMessage* msg)
{
    char s[MAX_MSG_LEN] = {0};
    srand(time(NULL) % 50 + getpid() + first_port%5);
    memset(msg, 0, sizeof(AMessage));   
    msg->sleep_time = 1 + rand() % 3;    
    msg->msg_len = 1 + rand() % MAX_MSG_LEN;
    printf("\n");
    for (int i = 0; i < msg->msg_len; i++) {
        s[i] = 'a' + rand() % 26;
        printf("%c", s[i]);
    }
    msg->msg = s;
    printf("\n");
}


int main(int argc, const char* argv[])
{
    AMessage msg = AMESSAGE__INIT; // AMessage
    void* buf; // Buffer to store serialized data
    unsigned len; // Length of serialized data


    msg_create(&msg);

    len = amessage__get_packed_size(&msg);
    buf = malloc(len);
    amessage__pack(&msg, buf);

    fprintf(stderr, "Writing %d serialized bytes\n", len); // See the length of message
    fwrite(buf, len, 1, stdout); // Write to stdout to allow direct command line piping

    free(buf); // Free the allocated serialized buffer
    return 0;
}
